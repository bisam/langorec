Comment j'ai commencé à apprendre l'espagnol
############################################

:date: 2023-01-29 12:00
:tag: apprendre, aprender
:category: Apprendre
:slug: comment-j-ai-commence-a-apprendre-l-espagnol
:status: draft

Je vais essayer de résumer ici comment je m'y suis pris pour essayer d'apprendre
l'espagnol. J'ai commencé il y a environ un an (au printemps 2022). Mais je
crois qu'il faut déjà que je digresse pour rembobiner la cassette du temps.

Avant d'apprendre l'espagnol
----------------------------

Ma langue maternelle c'est le français. J'ai appris l'anglais et l'allemand à
l'école. Ça ne m'a pas laissé beaucoup de souvenirs. Si je m'en sors avec
l'anglais c'est que pour trouver la réponse à un problème informatique, il n'y a
pas d'autres solutions que de chercher dans cette langue. Donc j'étais en
immersion sur internet. Ce séjour linguistique est principalement écrit et
asymétrique : j'arrive assez bien à comprendre mais m'exprimer, c'est autre
chose.

L'allemand était tombé dans une désuétude indolente et puis, un jour, on m'a
montré Duolingo. Je me suis laissé gagner par l'aspect ludique de l'application
et j'ai un peu pratiqué. J'ai même beaucoup pratiqué, ça devait assez obsédant.
Mais bon, Duolingo a des défauts et j'ai fini pas me lasser des répétions, des
erreurs de traduction sans importance pour être compris par des humains mais que
Duolingo me refusait. J'ai abandonné.

Et puis on m'a parlé d'un livre sur l'apprentissage des langues. J'y ai jeté un
œil. C'était `Fluent forever
<https://annas-archive.org/search?q=fluent+forever>`_, un livre qui doit dire à
peu près la même chose que tant d'autres mais pour moi c'était complètement
nouveau. Ça impliquait d'ailleurs plus que le simple fait d'apprendre une langue
mais je laisse ça de côté. Tout l'objectif du livre me semble d'être la vente
des services d'une entreprise du même nom.

Reste que le livre a ouvert la boîte de pandore avec ce truc qui s'appelle `Anki
<https://apps.ankiweb.net>`_. J'y reviendrais peut-être plus tard, il s'agit en
résumé de réviser régulièrement avec un système de "flashcard", des sortes de
questions-réponses. J'ai eu un peu de mal, comme tout le monde et puis je suis
devenu accro.  Fabriquer des flashcards, les passer en revue, naviguer dans ma
mémoire comme si c'était un grand registre, ... C'était passionnant, plus ludique
encore que Duolingo.

J'ai emmagasiné beaucoup de vocabulaire et puis je me suis retrouvé perdu dans
mon apprentissage et j'ai lâché l'affaire. Scheiße! Quelques temps après j'ai
commencé l'italien pour patiner à nouveau. Merda! Et puis voilà que l'espagnol vint
frapper à ma porte. ¡Dios mío!

Pour commencer
--------------

J'avais pu tester pas mal de choses avec l'allemand et l'italien. J'avais
commencé à apprendre un peu l'alphabet phonétique. Donc j'allais appliquer une
méthode qui venait principalement de Fluent forever :

* collecter des livres de grammaire et des guides de conversation
* étudier précisément la prononciation, l'écriture, utiliser des flashcards pour
  apprendre les règles avec des exemples, distinguer des paires minimales.
* dresser une liste des mots les plus fréquents
* créer les cartes correspondantes dans Anki
* parallèlement, étudier la conjugaison afin de pouvoir apprendre toutes les
  formes conjuguées de verbes modèles réguliers (habler, deber, vivir) et
  irréguliers (ser, sentir, pensar)
* parallèlement apprendre d'autres types de contenus qui permettent de se
  plonger dans l'univers, de la géographie, des chansons
* réutiliser un deck Anki qui s'appelle `Ultimate Spanish Conjugation
  <http://www.asiteaboutnothing.net/w_ultimate_spanish_conjugation.html>`_ dont
  j'avais déjà pu apprécier la qualité en apprenant l'italien.
* étudier la grammaire et créer des cartes en rapport avec les règles, des
  exemples
* ajouter des phrases pour placer le vocabulaire dans son contexte
* trouver quelque chose à lire à la hauteur de mes connaissances
* écrire des textes à partir du vocabulaire et des phrases que je connais
* pratiquer à l'oral avec des locuteurs

Ça c'était le plan, ça ne s'est pas vraiment déroulé comme ça. En écrivant ces
lignes, je vois que ça demanderait beaucoup d'explications. Je pense que ça peut 
être intéressant mais je vais devoir faire beaucoup d'autres articles.
Actuellement j'en suis encore à travailler la conjugaison et la grammaire.
J'apprends des phrases et j'écris des textes. La pratique orale je n'y suis pas
encore.

Les grandes idées que j'essaye de suivre
----------------------------------------

Avant de rentrer dans le détail voici quelques idées que j'essaye d'appliquer :

Faut s'marrer
^^^^^^^^^^^^^

J'ai besoin que les choses soient ludiques pour pratiquer quotidiennement. De
fait ça l'est et ça l'est tellement que ne pas réviser me fout mal. Il se trouve
qu'après plusieurs mois d'apprentissage j'ai merdé avec l'application AnkiDroid
que j'utilise pour passer en revue mes cartes ce qui faisait que je ne pouvais
passer en revue mes cartes sans me coller derrière un ordi. Le temps que je
résolve le problème, j'ai passé quelques jours assez pénibles.

Travailler l'oral au maximum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

J'essaye d'avoir la bonne prononciation en tête, d'utiliser des sons prononcés
par des locuteurs et si possible assez variés. Ça force à rentrer dans la
variété de la langue. D'autant qu'avec l'espagnol, les possibilités sont assez
grandes à travers les continents où il est parlé.

Ne pas chercher à traduire
^^^^^^^^^^^^^^^^^^^^^^^^^^

Bien sûr, intérieurement je passe beaucoup de temps à traduire du français à
l'espagnol et inversement. Mais j'essaye d'éviter ça au maximum en apprenant de
préférence avec des images, des définitions en espagnol lorsque c'est plus
conceptuel voire une traduction en anglais pour les phrases. C'est imparfait
mais mieux que rien, et ça rend les choses plus ludiques.

À présent
---------

J'ai évoqué plus haut l'état actuel de mon apprentissage. Je ne prétends pas que
a méthode est bonne, efficace ou quoi que ce soit. La seule qualité que je lui
trouve est qu'elle me motive à continuer chaque jour. Je la détaille ici pour
en garder une trace, peut-être qu'un jour viendra où je voudrais l'évaluer, me
rappeler par où je suis passé ? Je vais donc continuer à écrire sur ce sujet.
Certains articles reviendront sur ce qui s'est passé avant cette fin janvier
2023 où je frappe ces mots. D'autres s'intéresseront à l'apprentissage qui est
en cours.


Petit état des lieux jusqu'ici
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour finir, je vais simplement essayer de noter la temporalité avec laquelle les
choses se sont déroulées d'après les dates de création de mes cartes. Ça ne
présente peut-être que peu d'intérêt pour le lecteur mais faut que je me note ça
quelque part et quelque part c'est ici :

* **début avril 2022** : je sais que j'ai ajouté quelques cartes de géographie donc
  j'avais forcément révisé les bases de la prononciation.
* **en mai** :

  * début mai, le vocabulaire le plus fréquent est ajouté (mais pas encore étudié)
  * **mi-mai, je commence enfin l'étude du vocabulaire** et je rajoute quelques
    règles de prononciation telles que: « Comment prononce-t-on ⟨v⟩ comme dans
    vivir ? » / « Pareil qu'un ⟨b⟩ comme dans /biˈβiɾ/ »
  * **fin mai, j'attaque la conjugaison** : le nom des temps conjugués, principes de
    construction des terminaisons, les verbes modèles : hablar, deber et vivir.
    À partir de là, le rythme est d'environ 3 temps par verbes et une dizaine de
    mots de vocabulaire par jour. Voir moins.

* **fin juillet** : le rythme des conjugaisons s'intensifie, j'apprends un verbe
  par jour. J'y reviendrais surement en expliquant comme j'ai appris les verbes
  mais ça a duré 2 semaines maximum.

* **mi-aout : je commence à ajouter des phrases** à mon carquois, environ 5 par jour
* **début septembre : j'en ai fini avec la conjugaison**
* **tout le mois de septembre je n'apprends quasiment aucun nouveau mot de
  vocabulaire**
* **début octobre** : le rythme redevient normal
* **mi-octobre** : j'ajoute quelques règles relatives à la conjugaison. Par
  exemple : "Généralement, on accentue les verbes conjugués sur la première
  voyelle de la terminaison." C'est aussi le début de **l'ajout en masse de
  phrases** avec seulement une traduction anglaise, parfois mauvaise. Le rythme
  est d'environ 20 phrases par jour (ex: "Soy yo").
* **mi-novembre** : je n'ajoute plus de vocabulaire, pour diverses raisons
* **mi-décembre** : j'ajoute du vocabulaire dans le but de **lire mon premier livre**
* **janvier 2023** : **j'écris mes premiers textes**, je rajoute du vocabulaire pour
  lire un autre livre et je crée de nouvelles cartes, notamment des paires de
  mots tels que "el/él" pour bien les distinguer. J'ajoute aussi des règles sur
  l'accentuation.

On voit que je n'applique pas ma méthode puisque au moins 6 mois après avoir
commencé, j'en suis encore aux règles de prononciation. Je développerais
peut-être mais il y a une sorte de va-et-vient nécessaire pour comprendre ce
qu'il faut apprendre. J'ai compris l'intérêt de distinguer el et él après de
nombreuses corrections reçues en écrivant en espagnol.

À suivre donc.
