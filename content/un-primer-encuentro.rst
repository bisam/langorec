Un primer encuentro
####################

:date: 2023-01-29
:tag: hombre, playa, feliz año
:category: Escenas de vida
:corrector: Lii
:langcorrect: https://langcorrect.com/journal/un-primero-encuentro

Me gustaría empezar este diario con una pequeña historia.

Hace un par de días, me encontraba cerca de la playa. El sol estaba alto en
inverno pero no tanto como en el verano entonces el sol estaba bajo, pero
bastante caliente. Un hombre pasó y me dijo :

* "¡Feliz año! ¿cómo estas?"
* “Estoy bien, gracias” respondí. “¿Y tú?” le pregunté.
* “Estoy bien, pero la vida ... es muy compleja. Hay muchas cosas ..."

Hablamos un rato y el hombre se fue a su coche. Me pareció muy preocupado pero
no supe por qué. Espero que nuestra conversación le haya gustado. Ya no sé como
se siente ahora. Así son las cosas, estaba solo en un corto encuentro.

Después de esta charla, pensaba que la debería escribir aquí, en LangCorrect.
Hace un par de días, lo hago solo hoy, ¡pero ya lo hago! Quería continuar a
contando este tipo de historias. Creo que hay un poco de poesía. A ver.

