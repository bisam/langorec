Como he empezado a aprender español
###################################

:date: 2023-02-02 09:00
:tag: apprendre, aprender
:category: Apprendre
:lang: es
:slug: comment-j-ai-commence-a-apprendre-l-espagnol
:status: draft

Aquí voy a tratar de explicar como he intentado de aprender español. He comencé
hace un año, desde la primavera 2022. Pero creo que debo volver un poco más en
el pasado y voy a hablar del español después.

Antes la ensenanza del espanol
------------------------------

Mi primer idioma es el francés. En la escuela, aprendí inglés y también alemán.
No tengo muchas cosas que recordar de este periodo. Si he acabado un poco a leer
inglés es porque buscando la solución a un problema con el computador, no tengo
otra forma que buscarla en este idioma. Así es como si estoy viviendo en un país
donde todo el mundo habla inglés. Pero es solo un país virtual. Si acabo de leer,
es mas difícil de escribir y hablar...

Olvidé todo acerca del alemán pero, un día, hay alguien que me habló de
Duolingo. Me gustó que fue al igual que un juego y comencé a lo aprender
otra vez. Me gustó mucho y fue un nuevo hábito en todos los días. Pero Duolingo
tiene problemas y al final, no me gustó tanto. No quise que tenía ver siempre lo
mismo, siempre las faltas de traducción que en verdad no importa. Abandoné
Duolingo.

Pero me pareció triste que me detuve a continuar. Un día, una amiga me avisó
sobre un libro para aprender los idiomas. Lo he leí. Se llama `Fluent forever
<https://annas-archive.org/search?q=fluent+forever>`_, este libro debe decir lo
mismo que otros libros pero en este momento fue algo muy nuevo. Fue mas que
aprender solo un idioma, pero no es el sujeto de este texto. Creo que la meta
del libro es la vente de productos de un compañía con lo mismo nombre. No me
interesa.

Este libro fue el principio de mas descubrimientos. Él hablaba de un programa
llamando `Anki <https://apps.ankiweb.net>`_. Tal vez hablaría de eso mas tarde,
es un sistema de "flashcards", algo con preguntas y respuestas. Las primeras
veces fueron muy difícil, como todo el mundo, pero me he convertido. Hacer las
flashcards, volver a verlos, tratar de leer en mi memoria como leo en un grande
libro ... Fue fascinante y mucho mas un juego que Duolinguo.

Aprendí mucho vocabulario y un día me perdí en todas las palabras y abandoné
todo. ¡Scheiße! Un par de meses después, empiezo con el italiano pero no logré
de nuevo. Merda! Y un otro día, ¿quién estaba a la puerta de mí cabeza? ¡Hola Español!

Pour commencer
--------------

J'avais pu tester pas mal de choses avec l'allemand et l'italien. J'avais
commencé à apprendre un peu l'alphabet phonétique. Donc j'allais appliquer une
méthode qui venait principalement de Fluent forever :

* collecter des livres de grammaire et des guides de conversation
* étudier précisément la prononciation, l'écriture, utiliser des flashcards pour
  apprendre les règles avec des exemples, distinguer des paires minimales.
* dresser une liste des mots les plus fréquents
* créer les cartes correspondantes dans Anki
* parallèlement, étudier la conjugaison afin de pouvoir apprendre toutes les
  formes conjuguées de verbes modèles réguliers (habler, deber, vivir) et
  irréguliers (ser, sentir, pensar)
* parallèlement apprendre d'autres types de contenus qui permettent de se
  plonger dans l'univers, de la géographie, des chansons
* réutiliser un deck Anki qui s'appelle `Ultimate Spanish Conjugation
  <http://www.asiteaboutnothing.net/w_ultimate_spanish_conjugation.html>`_ dont
  j'avais déjà pu apprécier la qualité en apprenant l'italien.
* étudier la grammaire et créer des cartes en rapport avec les règles, des
  exemples
* ajouter des phrases pour placer le vocabulaire dans son contexte
* trouver quelque chose à lire à la hauteur de mes connaissances
* écrire des textes à partir du vocabulaire et des phrases que je connais
* pratiquer à l'oral avec des locuteurs

Ça c'était le plan, ça ne s'est pas vraiment déroulé comme ça. En écrivant ces
lignes, je vois que ça demanderait beaucoup d'explications. Je pense que ça peut 
être intéressant mais je vais devoir faire beaucoup d'autres articles.
Actuellement j'en suis encore à travailler la conjugaison et la grammaire.
J'apprends des phrases et j'écris des textes. La pratique orale je n'y suis pas
encore.

Les grandes idées que j'essaye de suivre
----------------------------------------

Avant de rentrer dans le détail voici quelques idées que j'essaye d'appliquer :

Faut s'marrer
^^^^^^^^^^^^^

J'ai besoin que les choses soient ludiques pour pratiquer quotidiennement. De
fait ça l'est et ça l'est tellement que ne pas réviser me fout mal. Il se trouve
qu'après plusieurs mois d'apprentissage j'ai merdé avec l'application AnkiDroid
que j'utilise pour passer en revue mes cartes ce qui faisait que je ne pouvais
passer en revue mes cartes sans me coller derrière un ordi. Le temps que je
résolve le problème, j'ai passé quelques jours assez pénibles.

Travailler l'oral au maximum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

J'essaye d'avoir la bonne prononciation en tête, d'utiliser des sons prononcés
par des locuteurs et si possible assez variés. Ça force à rentrer dans la
variété de la langue. D'autant qu'avec l'espagnol, les possibilités sont assez
grandes à travers les continents où il est parlé.

Ne pas chercher à traduire
^^^^^^^^^^^^^^^^^^^^^^^^^^

Bien sûr, intérieurement je passe beaucoup de temps à traduire du français à
l'espagnol et inversement. Mais j'essaye d'éviter ça au maximum en apprenant de
préférence avec des images, des définitions en espagnol lorsque c'est plus
conceptuel voire une traduction en anglais pour les phrases. C'est imparfait
mais mieux que rien, et ça rend les choses plus ludiques.

À présent
---------

J'ai évoqué plus haut l'état actuel de mon apprentissage. Je ne prétends pas que
a méthode est bonne, efficace ou quoi que ce soit. La seule qualité que je lui
trouve est qu'elle me motive à continuer chaque jour. Je la détaille ici pour
en garder une trace, peut-être qu'un jour viendra où je voudrais l'évaluer, me
rappeler par où je suis passé ? Je vais donc continuer à écrire sur ce sujet.
Certains articles reviendront sur ce qui s'est passé avant cette fin janvier
2023 où je frappe ces mots. D'autres s'intéresseront à l'apprentissage qui est
en cours.


Petit état des lieux jusqu'ici
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour finir, je vais simplement essayer de noter la temporalité avec laquelle les
choses se sont déroulées d'après les dates de création de mes cartes. Ça ne
présente peut-être que peu d'intérêt pour le lecteur mais faut que je me note ça
quelque part et quelque part c'est ici :

* **début avril 2022** : je sais que j'ai ajouté quelques cartes de géographie donc
  j'avais forcément révisé les bases de la prononciation.
* **en mai** :

  * début mai, le vocabulaire le plus fréquent est ajouté (mais pas encore étudié)
  * **mi-mai, je commence enfin l'étude du vocabulaire** et je rajoute quelques
    règles de prononciation telles que: « Comment prononce-t-on ⟨v⟩ comme dans
    vivir ? » / « Pareil qu'un ⟨b⟩ comme dans /biˈβiɾ/ »
  * **fin mai, j'attaque la conjugaison** : le nom des temps conjugués, principes de
    construction des terminaisons, les verbes modèles : hablar, deber et vivir.
    À partir de là, le rythme est d'environ 3 temps par verbes et une dizaine de
    mots de vocabulaire par jour. Voir moins.

* **fin juillet** : le rythme des conjugaisons s'intensifie, j'apprends un verbe
  par jour. J'y reviendrais surement en expliquant comme j'ai appris les verbes
  mais ça a duré 2 semaines maximum.

* **mi-aout : je commence à ajouter des phrases** à mon carquois, environ 5 par jour
* **début septembre : j'en ai fini avec la conjugaison**
* **tout le mois de septembre je n'apprends quasiment aucun nouveau mot de
  vocabulaire**
* **début octobre** : le rythme redevient normal
* **mi-octobre** : j'ajoute quelques règles relatives à la conjugaison. Par
  exemple : "Généralement, on accentue les verbes conjugués sur la première
  voyelle de la terminaison." C'est aussi le début de **l'ajout en masse de
  phrases** avec seulement une traduction anglaise, parfois mauvaise. Le rythme
  est d'environ 20 phrases par jour (ex: "Soy yo").
* **mi-novembre** : je n'ajoute plus de vocabulaire, pour diverses raisons
* **mi-décembre** : j'ajoute du vocabulaire dans le but de **lire mon premier livre**
* **janvier 2023** : **j'écris mes premiers textes**, je rajoute du vocabulaire pour
  lire un autre livre et je crée de nouvelles cartes, notamment des paires de
  mots tels que "el/él" pour bien les distinguer. J'ajoute aussi des règles sur
  l'accentuation.

On voit que je n'applique pas ma méthode puisque au moins 6 mois après avoir
commencé, j'en suis encore aux règles de prononciation. Je développerais
peut-être mais il y a une sorte de va-et-vient nécessaire pour comprendre ce
qu'il faut apprendre. J'ai compris l'intérêt de distinguer el et él après de
nombreuses corrections reçues en écrivant en espagnol.

À suivre donc.
