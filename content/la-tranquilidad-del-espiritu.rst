La tranquilidad del espíritu
############################

:date: 2023-01-29 00:02
:tag: hombre, playa, feliz año
:category: Escenas de vida
:corrector: Maite
:langcorrect: https://langcorrect.com/journal/la-tranquilidad-del-espíritu

Esta mañana yo necesitaba un poco de aire. He viajado con mi coche hacia la
montaña. Esperaba encontrar un sitio tranquilo para descansar. Le camino fue muy
difícil, debí regresar pero al fin he encontrado una área recreativa. Hoy no
había ningunas nubes. Este lugar era perfecto, con una hermosa vista.

Era demasiado perfecto. Después un momento tranquilo, oí el ruido de una moto y
coches. Otras personas habían pensado como yo. Muy pronto, iniciaron una fiesta.
Creo que era el cumpleaños de alguien. Podía oír su música. No sé porqué pero me
gustó. Alguien cantaba a veces canciones bastante buenas.

Sólo nos hemos dicho "¡hola!" cuando pasé cerca de ellos. Fui a pasear un poco
por el bosque. He visto bellas aves que no conocía, con colores rojo y negro en
su plumaje. Después cuando volví al lugar anterior, la fiesta se había acabado.
Todo el mundo se estaba yendo.

Yo me quedé un rato allí. Ya no había nadie más. Me sentía extraño: por la
mañana quería un poco de tranquilidad y en este momento, me hubiese gustado que
siguiera la fiesta. Tal vez si hubiese estado allí el hombre de la playa, me
habría dicho en ese momento "¡La vida no tiene sentido!".
