Flores de noche
###############

:date: 2023-01-29 00:03
:tag: noche, flores, pensamientos
:category: Escenas de vida
:corrector: Maite, cristinaaquafina101

Soy yo otra vez, de noche, cruzando un puente. No hay nadie, sólo yo y mis
pensamientos. Estoy tan poco concentrado que es como si mis pensamientos
estuvieran pensando por ellos mismos, y viajando a mi lado. Todo está en
silencio. Solo se puede oír el sonido de mis pasos ni el ruido de mis
pensamientos.

Una visión me golpea repentinamente. Al lado del camino, bajo una luz, alguien
ha puesto unas flores amarillas. Son flores de plástico, imagino que son en
recuerdo de un ser querido. Es una idea un poco triste.

Yo pienso en mi gente : los vivos y los muertos y los viejos y los jóvenes. Dejo
las flores allí pero llevo un poco de tristeza conmigo. Y sigo adelante en la
noche.
