First note and why not ?
########################

:slug: premier-texte-et-intentions
:lang: en
:tags: meta
:category: meta

I want to write here some Spanish texts. I'm trying to learn this language
and sometimes I write something in `LangCorrect <https://langcorrect.com>`_.
I wanted to list them somewhere. Here it is. So the Spanish texts here are the
result of the original and the modifications I made after some people tried to
correct them. Maybe there is some mistake in the new version but I think I will
let them like that.

I try to write something interesting even if I have a limited vocabulary. I don't
know if it's something anyone would read but I post them anyway.

Maybe I will speak about how I learn Spanish ? How correcting someone's french
help me to understand my mother tongue ? Maybe I will talk about tools like Anki.
Maybe I will start to learn English for real ??? I don't know yet. This blog is
mainly written in french and some text will be translated in English. Maybe
Spanish too ?

If you want to write me an email, use my username (bisam), an @ and the root
domain name (r4.re).
