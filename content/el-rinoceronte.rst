El rinoceronte
##############

:date: 2023-01-29 00:07
:tag: animal, sueño
:category: Sueños 
:corrector: critinaaquafina101, Fernanhaiku

¡He soñado!

Era una fiesta familiar, pero había muchas personas que no eran de mi familia,
ni que conozca. Nadie había cocinado y todos querían comer. Discutíamos
cuando pregunté por qué haríamos ensaladas. Es fácil, pero todo el mundo
siguió hablando y tuve que hacer las ensaladas solo.

Era el departamento de mis padres (en realidad, nunca he visto este lugar).
Todos estaban ocupados, vi precisamente a mi hermano y alguien que estaba
escribiendo en la computadora. No podía entender lo que estaba escrito por que
era coreano. No hablo coreano, es lo mismo en este sueño. Él me preguntó sobre
la electricidad ¿230V? ¿USB? ¿5V? ¿CC-CC?. No sé el final.

Era un aparcamiento. Estaba sorprendido: los baños parecían muy viejo. Había
alguien dentro. Me largué de ahí y un poco más luego, lo vi. Estaba él. Era
hermoso. Muy hermoso. ¡El rinoceronte! Pero, necesitaba cubrirme por qué el
rinoceronte no me viera. Traté de irme a un lado pero había tantas raíces. No
había visto todos esos árboles adelante de mí. Traté a un otro lado, traté de
mover la tierra del suelo, estaba muy difícil. Lo estaba logrando cuando entendí
que el rinoceronte no quería atacarme.

Y abrí los ojos.
