¿Quien estaba?
####################

:date: 2023-01-29 00:04
:tag: cabras 
:category: Escenas de vida
:corrector: Fernanhaiku

Ya fue hace un par de horas que yo paseaba entre el sendero de rocas, buscando
mi camino. A veces había unos arboles pero el suelo estaba duro y seco. No había
nadie. Repentinamente, después en una pequeña colina, vi algo se mover … ¡o
alguien!

Enfrente de mi, pude ver un tipo de cabra. No estoy seguro de lo que he visto.
En mi mente, estaba preguntándome “¿quien es esta cabra?” y el sujeto de mi
pregunta dijo “¿quien es este cabrón?”. No lo sé, pero ella me miró.

No me encuentro tan bien como fuera posible. Empecé a ver que la cabra tenía
compañeras. Estaban detrás de las rocas y la vegetación. He actuado como siempre
cada que veo un animal salvaje : con cuidado. No sabía que paso en sus cabezas,
solo sabía que no quería volver. La noche estaba acerca y tenía que seguir
mi camino. Pero no me gustó que crear mas miedo. Las cabras no me
parecieron agresivas pero no sabía si mi presencia les parecía pacifica. Me
detuve un poco sin mover entonces seguí adelante de a poco. Las cabras se
alejaron un poco. Hemos continuado así, tranquilo y me fui, sin decir
"¡Adiós!".

Ahora, no estoy seguro de que pasó. Ya no sé si estaban las cabras salvajes o
no. Espero que no les haya molestado.

